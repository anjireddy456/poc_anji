package com.makeMyTrip.suites;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.frameworkLib.DriverConfig;
import com.makeMyTrip.pageObjects.BasePageObjects;
import com.makeMyTrip.pages.BasePage;
import com.makeMyTrip.pages.HomePage;
import com.makeMyTrip.pages.LoginPage;
import com.makeMyTrip.pages.MyAccountPage;

public class TestExecution {
	WebDriver driver;

	@BeforeClass
	public void InitDriver() {
		driver = DriverConfig.launchDriver(driver, DriverConfig.getPropertyValueFromConfig("browserName"));
	}

	@AfterClass
	public void Quit() {
		driver.quit();
	}

	@Test(priority = 0, description = "Login with valid data")
	public void LoginVerification() {
		BasePage.getMakeMyTripUrl(driver);
		BasePage.clickOnLoginBtn(driver);
		LoginPage.login(driver, DriverConfig.getPropertyValueFromConfig("username"),
				DriverConfig.getPropertyValueFromConfig("Password"));
		HomePage.clickOnProfileBtn(driver);
		HomePage.clickOnMyAccount(driver);
		MyAccountPage.clickOnProfileOption(driver);
		Assert.assertEquals(DriverConfig.getPropertyValueFromConfig("username"),
				MyAccountPage.getProfileMailID(driver));
	}

	@Test(priority = 1, description = "Searching the buses")
	public void SearchBus() {
		driver.navigate().to(BasePageObjects.makeMyTripUrl);
		HomePage.clickOnBusTab(driver);
		HomePage.searchBus(driver, DriverConfig.getPropertyValueFromConfig("from"),
				DriverConfig.getPropertyValueFromConfig("to"));
		Assert.assertEquals(DriverConfig.getPropertyValueFromConfig("from"), HomePage.getModifyFromTxt(driver));
		Assert.assertEquals(DriverConfig.getPropertyValueFromConfig("to"), HomePage.getModifyToTxt(driver));
	}

	@Test(priority = 2, description = "Price comparision for the first bus")
	public void PriceVerification() {
		String price = HomePage.getPriceFirstBus(driver);
		System.out.println(DriverConfig.getNumberFromString(price));
		HomePage.clickSelectFirstBus(driver);
		HomePage.clickSeat(driver);
		String price1 = HomePage.getSeatPrice(driver);

		System.out.println(DriverConfig.getNumberFromString(price1));

		Assert.assertEquals(DriverConfig.getNumberFromString(price), DriverConfig.getNumberFromString(price1));
	}

}
