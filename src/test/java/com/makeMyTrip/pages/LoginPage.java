package com.makeMyTrip.pages;

import org.openqa.selenium.WebDriver;

import com.frameworkLib.DriverConfig;
import com.makeMyTrip.pageObjects.LoginPageObjects;

public class LoginPage {
	
	
	
	public static void login(WebDriver driver,String userId,String password) {
		DriverConfig.setText(driver, LoginPageObjects.txtUserName, userId);
		DriverConfig.setText(driver, LoginPageObjects.txtPassword, password);
		DriverConfig.click(driver, LoginPageObjects.btnLogin);
	}

}
