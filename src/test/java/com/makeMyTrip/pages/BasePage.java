package com.makeMyTrip.pages;

import org.openqa.selenium.WebDriver;

import com.frameworkLib.DriverConfig;
import com.makeMyTrip.pageObjects.BasePageObjects;

public class BasePage {
	
public static void getMakeMyTripUrl(WebDriver driver) {
	driver.get(BasePageObjects.makeMyTripUrl);
}

public static void clickOnLoginBtn(WebDriver driver) {
	DriverConfig.click(driver,BasePageObjects.btnLogin);
}
}
