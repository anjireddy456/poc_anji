package com.makeMyTrip.pages;

import org.openqa.selenium.WebDriver;
import com.frameworkLib.DriverConfig;
import com.makeMyTrip.pageObjects.HomePageObjects;

public class HomePage {
	
	public static void clickOnProfileBtn(WebDriver driver) {
		DriverConfig.click(driver,HomePageObjects.btnProfileIcon);
	}
	public static void clickOnMyAccount(WebDriver driver) {
		DriverConfig.click(driver,HomePageObjects.btnMyAccount);
	}
	
	public static void clickOnBusTab(WebDriver driver) {
		DriverConfig.click(driver,HomePageObjects.tabBus);
	}
	
	public static void searchBus(WebDriver driver,String fromLocation, String toLocation) {
		DriverConfig.setText(driver,HomePageObjects.txtFrom,fromLocation);
		DriverConfig.setText(driver,HomePageObjects.txtTo,toLocation);
		DriverConfig.click(driver,HomePageObjects.btnSearch);
	}
	
	public static String getModifyFromTxt(WebDriver driver) {
		 return DriverConfig.getText(driver, HomePageObjects.btnModifyFrom);
	}
	
	public static String getModifyToTxt(WebDriver driver) {
		 return DriverConfig.getText(driver, HomePageObjects.btnModifyTo);
	}
	
	public static String getPriceFirstBus(WebDriver driver) {
		 return DriverConfig.getText(driver, HomePageObjects.txtBusPriceFirst);
	}
	public static void clickSelectFirstBus(WebDriver driver) {
		DriverConfig.click(driver,HomePageObjects.btnBusSelectFirst);
	}
	
	public static void clickSeat(WebDriver driver) {
		DriverConfig.click(driver,HomePageObjects.btnSelectSeat);
	}
	
	public static String getSeatPrice(WebDriver driver) {
		
		 return DriverConfig.getText(driver, HomePageObjects.txtSeatPrice);
	}
}
