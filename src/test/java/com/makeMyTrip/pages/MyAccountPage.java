package com.makeMyTrip.pages;

import org.openqa.selenium.WebDriver;
import com.frameworkLib.DriverConfig;
import com.makeMyTrip.pageObjects.MyAccountPageObjects;

public class MyAccountPage {
	public static void clickOnProfileOption(WebDriver driver) {
		DriverConfig.click(driver,MyAccountPageObjects.btnProfile);
	}
	
	public static String getProfileMailID(WebDriver driver) {
		 return DriverConfig.getText(driver, MyAccountPageObjects.txtEmail);
	}
	
}
