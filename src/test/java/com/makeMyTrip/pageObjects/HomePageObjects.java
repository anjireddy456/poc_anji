package com.makeMyTrip.pageObjects;

import org.openqa.selenium.By;

public class HomePageObjects {
	
	public static By btnProfileIcon=By.id("ch_logged-in");
	public static By btnMyAccount=By.id("ch_logged-inaccount");
	public static By tabBus=By.id("header_tab_bus");
	public static By txtFrom=By.id("hp-widget__sfrom");
	public static By txtTo=By.id("hp-widget__sTo");
	public static By btnSearch=By.id("searchBtn");
	public static By btnModifyFrom=By.id("modify_frm_city_name");
	public static By btnModifyTo=By.id("modify_to_city_name");
	public static By btnBusSelectFirst=By.xpath("//*[@id=\"seatmap_btn_0\"]");
	public static By txtBusPriceFirst=By.xpath("//*[@id=\"bus_price_0\"]");
	public static By btnSelectSeat=By.xpath("/html/body/div[3]/div[3]/div[4]/div/div/div/div[2]/div[2]/div[1]/div[2]/div[2]/div/div/div/div[1]/div[2]/div[2]/div/p[1]/span");
	public static By linkPriceBreakup=By.xpath("/html/body/div[3]/div/div/div/div[3]/div/div[2]/div[1]/div[2]/div/div/div/div[2]/div/div/div[3]/div/span[1]/span[2]/span/span[2]");

	public static By txtSeatPrice=By.xpath("/html/body/div[3]/div[3]/div[4]/div/div/div/div[2]/div[2]/div[1]/div[2]/div[2]/div/div/div/div[2]/p[4]/span[2]");

	


}
