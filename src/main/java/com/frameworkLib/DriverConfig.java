package com.frameworkLib;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class DriverConfig {
	final static Logger logger = Logger.getLogger(DriverConfig.class);
	public static String Dir = System.getProperty("user.dir");
	public static String log4jConfigFile = Dir + "/src/main/resources/log4j.xml";
	public static int defaultWaitTime = 60;
	public static int defaultWaitTimeForJS = 30;
	public static String highlightElement = "yes";
	public static String configPropertyFilePath = Dir + "/src/main/resources/Config.properties";

	/**
	 * 
	 * @param propertFilePath
	 * @param propertyFileKey
	 * @return
	 */
	public static String getPropertyValue(String propertFilePath, String propertyFileKey) {
		String propertyFileValue = "Please check property file key";
		InputStream is = null;
		Properties prop = null;
		try {
			prop = new Properties();
			is = new FileInputStream(new File(propertFilePath));
			prop.load(is);
			propertyFileValue = prop.getProperty(propertyFileKey);

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return propertyFileValue;
	}

	/**
	 * 
	 * @param propertyFileKey
	 * @return
	 */
	public static String getPropertyValueFromConfig(String propertyFileKey) {
		return getPropertyValue(configPropertyFilePath, propertyFileKey);
	}

	/**
	 * 
	 * @param driver
	 * @param browserName
	 * @return
	 * @throws Exception
	 */
	public static WebDriver launchDriver(WebDriver driver, String browserName) {
		DOMConfigurator.configure(log4jConfigFile);
		if (browserName.equalsIgnoreCase("chrome")) {
			System.setProperty("webdriver.chrome.driver", Dir + "/libs/drivers/chromedriver.exe");
			driver = new ChromeDriver();
		} else if (browserName.equalsIgnoreCase("firefox")) {
			System.setProperty("webdriver.gecko.driver", Dir + "/libs/drivers/geckodriver.exe");
			driver = new FirefoxDriver();
			System.setProperty(FirefoxDriver.SystemProperty.DRIVER_USE_MARIONETTE, "true");
			System.setProperty(FirefoxDriver.SystemProperty.BROWSER_LOGFILE, Dir + "/libs/drivers/logs.txt");
		}
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		return driver;
	}

	/**
	 * 
	 * @param driver
	 * @param defaultWaitTimeForJS
	 * @return
	 */
	public static boolean waitForJStoLoad(WebDriver driver, int defaultWaitTimeForJS) {
		WebDriverWait wait = new WebDriverWait(driver, defaultWaitTimeForJS);
		// wait for jQuery to load
		ExpectedCondition<Boolean> jQueryLoad = new ExpectedCondition<Boolean>() {
			public Boolean apply(WebDriver driver2) {
				try {
					return ((Long) ((JavascriptExecutor) driver2).executeScript("return jQuery.active") == 0);
				} catch (Exception e) {
					return true;
				}
			}
		};
		ExpectedCondition<Boolean> jsLoad = new ExpectedCondition<Boolean>() {
			public Boolean apply(WebDriver driver3) {
				return ((JavascriptExecutor) driver3).executeScript("return document.readyState").equals("complete");
			}
		};
		return wait.until(jQueryLoad) && wait.until(jsLoad);
	}

	/**
	 * 
	 * @param driver
	 * @param by
	 * @return
	 */
	public static WebElement element(WebDriver driver, By by) {
		logger.info("*********************************************************************************************");
		WebElement Element = null;
		WebDriverWait wait = new WebDriverWait(driver, defaultWaitTime);
		if (by.toString().contains("web")) {
			waitForJStoLoad(driver, defaultWaitTimeForJS);
		}
		try {
			wait.until(ExpectedConditions.visibilityOfElementLocated(by));
		} catch (Exception e) {
			logger.info("locator not found and locator is : " + by);
		}
		Element = driver.findElement(by);
		if (highlightElement.equalsIgnoreCase("yes")) {
			try {
				if (driver instanceof JavascriptExecutor) {
					((JavascriptExecutor) driver).executeScript("arguments[0].style.border='3px solid red'", Element);
				}
			} catch (Exception e) {
				logger.info("Hiligghting the elemnt is not possible for : " + Element);
			}
		}

		return Element;
	}

	/**
	 * 
	 * @param driver
	 * @param by
	 */
	public static void submit(WebDriver driver, By by) {
		logger.info(" Performing Submit On " + by + " Elelemt");
		element(driver, by).submit();
	}

	/**
	 * 
	 * @param driver
	 * @param by
	 */
	public static void click(WebDriver driver, By by) {
		logger.info(" Performing Click On " + by + " Elelemt");
		element(driver, by).click();
	}

	/**
	 * 
	 * @param driver
	 * @param by
	 */
	public static void click_JS(WebDriver driver, By by) {
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("arguments[0].click();", element(driver, by));
	}

	/**
	 * 
	 * @param driver
	 * @param by
	 */
	public static void clickByActions(WebDriver driver, By by) {
		Actions actions = new Actions(driver);
		actions.moveToElement(element(driver, by));
		actions.click();
		actions.perform();
	}

	/**
	 * 
	 * @param driver
	 * @param by
	 */
	public static void clear(WebDriver driver, By by) {
		logger.info(" Performing Clear On " + by + " Elelemt");
		element(driver, by).clear();
	}

	/**
	 * 
	 * @param driver
	 * @param by
	 * @param value
	 */
	public static void setText(WebDriver driver, By by, String value) {
		try {
			click(driver, by);
			clear(driver, by);
		} catch (Exception e) {
			logger.info(e);
		}
		logger.info(" Performing SendKeys On " + by + " Elelemt");
		element(driver, by).sendKeys(value);
	}

	/**
	 * 
	 * @param by
	 * @return
	 */
	public static String getTextInnerText(WebDriver driver, By by) {
		String s = "";
		s = element(driver, by).getAttribute("innerText");
		return s;
	}

	/**
	 * 
	 * @param by
	 * @return
	 */
	public static String getText(WebDriver driver, By by) {
		String s = element(driver, by).getText();
		return s;
	}

	/**
	 * 
	 * @param by
	 * @return
	 */
	public static String getTextOptional(WebDriver driver, By by) {
		String s = "";
		try {
			s = element(driver, by).getText();
		} catch (Exception e) {
			logger.info("Text empty for :" + by);
		}
		return s;
	}
	/**
	 * 
	 * @param StringValue
	 * @return
	 */
	public static String getNumberFromString(String StringValue) {
		String str = StringValue;
		String numberOnly = str.replaceAll("[^0-9]", "");
		return numberOnly;
	}
}
